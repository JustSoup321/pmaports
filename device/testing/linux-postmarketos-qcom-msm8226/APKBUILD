# Maintainer: Newbyte <newbie13xd@gmail.com>
# Kernel config based on: arch/arm/configs/qcom_defconfig

_flavor="postmarketos-qcom-msm8226"
pkgname=linux-$_flavor
pkgver=6.8.1
pkgrel=0
_tag="v${pkgver/_/-}-msm8226"
pkgdesc="Mainline kernel fork for Qualcomm APQ8026/MSM8226/MSM8926 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
"
makedepends="
	bison
	findutils
	flex
	gmp-dev
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
"

# Source
source="
	linux-$_tag.tar.gz::https://github.com/msm8226-mainline/linux/archive/refs/tags/$_tag.tar.gz
	config-$_flavor.armv7
	"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
b15d4582c47c7615ced0d9bdc958bccbdca0fa00299b51e28a742bd30708f7c52d2e6130e1083616dc9f183dc9a844b433ab48610716750683d6653edc4b8dac  linux-v6.8.1-msm8226.tar.gz
cc4aff13de9291c1d4f31db5c1bde487d0502bce5c71482583ec30c38556c38347ef67133bd6fc159a7110f33cb80b1062b4013f63399da188053e6b89f33709  config-postmarketos-qcom-msm8226.armv7
"
